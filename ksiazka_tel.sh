#!/bin/bash
clear
if [ "$1" = "-h" ]
then
cat help_tel.txt | less
fi
echo -e "To jest ksiazka telefoniczna\n"

numer="1"
until [ "$numer" = "0" ]
do
echo -e "Co chcesz zrobic?\n"
echo -e "\t1. Dodac nowy kontakt;"
echo -e "\t2. Wyswietl dane;"
echo -e "\t3. Sortuj dane;"
echo -e "\t4. Wyszukaj kontakt;"
echo -e "\t9. Wyswietl pomoc;"
echo -e "\t0. Wyjscie z programu\n"
echo "Podaj numer (1,2,3,4,5,9 lub 0):"

read numer

case $numer in

	1) ./dodawanie_danych.sh
	;;
	2) cat dane.txt | less
	;;
	3) 
		sort -d dane.txt
	;;
	4)
	./wyszukiwanie.sh
	;;
	0) echo "Dziekuje za skorzystanie z naszego programu"
	;;
	9) cat help_tel.txt | less
	;;
	*) echo "Zly wybor";;
esac
done
